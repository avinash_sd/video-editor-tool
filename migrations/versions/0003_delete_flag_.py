"""empty message

Revision ID: 0003_delete_flag
Revises: 0002_file_metadata
Create Date: 2017-02-09 18:23:08.597625

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '0003_delete_flag'
down_revision = '0002_file_metadata'
branch_labels = None
depends_on = None


def upgrade():
    # commands auto generated by Alembic - please adjust! #
    op.add_column('files', sa.Column('is_deleted', sa.Boolean(), nullable=False, server_default="false"))
    # end Alembic commands #


def downgrade():
    # commands auto generated by Alembic - please adjust! #
    op.drop_column('files', 'is_deleted')
    # end Alembic commands #
