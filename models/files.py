from datetime import datetime

from sqlalchemy.ext.mutable import MutableDict
from sqlalchemy.dialects.postgresql import JSON

from .base import db
# from libs import get_random_string


class File(db.Model):
    """File table to store video file details."""

    __tablename__ = 'files'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Unicode, nullable=False)

    unique_id = db.Column(
        db.Unicode, default=None, nullable=False)
    meta_data = db.Column(MutableDict.as_mutable(JSON))
    status = db.Column(db.Unicode, default=u"UPLOADING", nullable=False)

    is_deleted = db.Column(db.Boolean, default=False, nullable=False)

    created_at = db.Column(
        db.DateTime, default=datetime.utcnow, nullable=False)
    modified_at = db.Column(db.DateTime,
                            default=datetime.utcnow,
                            onupdate=datetime.utcnow,
                            nullable=False)


class Languages(db.Model):
    """Language table to store locale_ids of different translations."""

    __tablename__ = 'languages'

    id = db.Column(db.Unicode, primary_key=True, nullable=False)
    name = db.Column(db.Unicode, nullable=False)
    created_at = db.Column(
        db.DateTime, default=datetime.utcnow, nullable=False)

    def __unicode__(self):
        """Unicode value."""
        return self.id

    def __repr__(self):
        """Repr value."""
        return self.__unicode__()
