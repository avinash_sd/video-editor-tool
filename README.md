## Technology stack
    * python 2.7
    * react
    * webpack
    * postgresql
    * celery
    * nginx
    * letsencrypt
    * docker-compose

## Steps to Run the project in docker

    $ docker-compose up
    $ docker-compose exec web python run.py db upgrade

## Command to access docker postgresql cli

    $ docker-compose exec app_db psql -U sdemos -d sandbox_authoring -W
