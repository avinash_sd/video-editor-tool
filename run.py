from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager, Shell

from libs import AddCacheBuster, CleanFiles, SyncFolder, create_app
from models import db

app = create_app()

manager = Manager(app)
Migrate(app, db)

manager.add_command('sync_folder', SyncFolder)
manager.add_command('db', MigrateCommand)
manager.add_command('clean-files', CleanFiles)
manager.add_command('cache-buster', AddCacheBuster)
manager.add_command('shell', Shell(make_context=lambda: {
    'app': app,
    'db': db
}))

if __name__ == '__main__':
    manager.run()
