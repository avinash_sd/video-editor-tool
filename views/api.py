import json
from math import ceil
import os
from tempfile import gettempdir

from flask import (
    Blueprint,
    current_app,
    jsonify,
    request,
    send_file
)

from models import File, Languages, db

api = Blueprint("api", __name__)


@api.route("/videos", defaults={"file_id": None})
@api.route("/videos/<file_id>", methods=["GET", "POST", "DELETE"])
def file_list(file_id):
    if file_id:
        file = File.query.filter(
            File.is_deleted.__eq__(False),
            File.unique_id == file_id
        ).first_or_404()

        if request.method == "POST":
            file.meta_data = request.json
            file.status = u'READY'
            db.session.add(file)
            db.session.commit()

        if request.method == "DELETE":
            file.is_deleted = True
            db.session.add(file)
            db.session.commit()
            return jsonify({
                "status": "DELETED"
            }), 200

        if request.args.get('download'):
            if file.status != 'UPLOAD_SUCCESS':
                return jsonify({
                    'message': 'DOWNLOAD ERROR - Upload file to download JSON'
                }), 404
            file_name = file.name.rsplit('.')[0]
            file_path = os.path.join(gettempdir(), file_name + '.json')
            if os.path.exists(file_path):
                os.remove(file_path)
            with open(file_path, 'w') as outfile:
                json.dump(file.meta_data, outfile)
            return send_file(file_path, as_attachment=True)

        languages = Languages.query.order_by(Languages.id).all()
        locale_id = request.cookies.get('locale_id', 'en_US')

        languages = [{
            "name": l.name,
            "id": l.id,
            "selected": (l.id == locale_id)
        } for l in languages] if languages else [{
            "name": "English",
            "id": locale_id,
            "selected": True
        }]

        file.meta_data['cdn_url'] = ('/static/media/{}/frames').format(file_id)
        return jsonify({
            "unique_id": file.unique_id,
            "name": file.name,
            "meta_data": file.meta_data,
            "languages": languages
        })

    page = request.args.get('page')
    page = int(page) if page else 1
    videos = File.query.filter(
        File.is_deleted.__eq__(False)
    )

    if request.args.get('searchBy'):
        search_column = request.args.get('searchBy')
        search_text = request.args.get('text')
        videos = videos.filter(
            getattr(File, search_column).ilike(u'%{}%'.format(search_text))
        )
    videos = videos.order_by(
        File.created_at.desc()
    )

    total_pages = int(ceil(videos.count() / float(10)))
    videos = videos.offset((page - 1) * 10).limit(10).all()

    return jsonify({
        "page": page,
        "total_pages": total_pages,
        "videos": [
            {
                "unique_id": f.unique_id,
                "name": f.name,
                "status": f.status,
                "created_at": f.created_at.isoformat()
            } for f in videos
        ]
    })


@api.route("/videos/search/", methods=['GET'])
def search():
    content = request.args.get("content")
    column_name = request.args.get("filter")
    files = File.query.filter(
        File.is_deleted.__eq__(False),
        getattr(File, column_name).ilike('%{}%'.format(content))
    ).with_entities(File.name, File.status, File.unique_id).all()

    return jsonify(files)


@api.route("/videos/<file_id>/upload-to-cdn/", methods=["POST"])
def upload_files_to_cdn(file_id):
    from tasks import create_video_files
    create_video_files.delay({'video_id': file_id})
    return jsonify({'status': 'UPLOADING'}), 200


@api.route("/videos/<file_id>/status")
def video_status(file_id):
    file = File.query.filter(
        File.is_deleted.__eq__(False),
        File.unique_id == file_id
    ).first_or_404()
    return jsonify({
        "unique_id": file.unique_id,
        "name": file.name,
        "status": file.status,
        "created_at": file.created_at.isoformat()
    })


@api.route('/video/upload', methods=['POST'])
def upload_video():
    """
    Create a unique folder for video.

    Save the video file and extract the video frames from the video file.
    """
    file = request.files.get('video')
    if not file or 'video' not in file.content_type:
        return jsonify({
            "status": "FAIL",
            "message": "Video file required"
        }), 400

    from libs import get_random_string
    unique_id = get_random_string()
    file_type = file.filename.split('.')[-1]
    file_name = unique_id + '.' + file_type
    file_path = os.path.join(current_app.config['MEDIA_FOLDER'], file_name)
    file.save(file_path)

    file_record = File()
    file_record.name = file.filename
    file_record.unique_id = unique_id

    file_record.status = u'PROCESSING'
    db.session.add(file_record)
    db.session.commit()

    from tasks import extract_frames
    extract_frames.delay({"id": file_record.id, "file_path": file_path})

    return jsonify({
        "status": "SUCCESS"
    }), 201
