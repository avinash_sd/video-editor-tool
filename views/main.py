import json
from math import ceil
import os
from tempfile import gettempdir

from flask import (
    Blueprint,
    abort,
    current_app,
    jsonify,
    redirect,
    request,
    render_template,
    send_file,
    url_for
)

from models import File, Languages, db

main = Blueprint("main", __name__)


@main.route('/')
@main.route('/page/<int:number>')
def home(number=1):
    """Render Sandbox Homepage with list of all files."""
    files = File.query.filter(
        File.is_deleted.__eq__(False)
    ).order_by(File.created_at.desc())
    total_pages = int(ceil(files.count() / float(10)))
    if total_pages and number > total_pages:
        return redirect(url_for('.home', number=total_pages))
    elif number <= 0:
        return redirect(url_for('.home'))

    files = files.offset((number - 1) * 10).limit(10).all()
    return render_template(
        'home.html',
        **dict(
            files=files,
            page_number=number,
            total_pages=total_pages
        )
    )


@main.route('/edit/<video_id>')
def author_video(video_id):
    file = File.query.filter(
        File.is_deleted.__eq__(False),
        File.unique_id == unicode(video_id)
    ).first_or_404()

    return render_template('edit.html', file=file)
