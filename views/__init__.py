from .api import api as api_bp
from .main import main as main_bp

__all__ = ["api_bp", "main_bp"]
