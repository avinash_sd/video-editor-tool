import os
import random
import shutil

from flask import current_app


def cleanup_assets(file_obj):
    folder_path = os.path.join(
        current_app.config['MEDIA_FOLDER'],
        file_obj.unique_id
    )
    shutil.rmtree(folder_path, ignore_errors=True)

    file_name = file_obj.unique_id + '.' + file_obj.name.split('.')[1]
    file_path = os.path.join(current_app.config['MEDIA_FOLDER'], file_name)
    if os.path.exists(file_path):
        os.remove(file_path)


def create_directories(folder):
    frames_folder = os.path.join(folder, 'frames')
    if not os.path.exists(frames_folder):
        os.makedirs(frames_folder)

    videos_folder = os.path.join(folder, 'videos')
    if not os.path.exists(videos_folder):
        os.makedirs(videos_folder)

    return frames_folder, videos_folder


def get_random_string(length=6, include_uppercase=False):
    alnumeric = '0123456789abcdefghijklmnopqrstuvwxyz'
    if include_uppercase:
        alnumeric += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

    random_str = ''.join(random.choice(alnumeric) for i in range(length))
    return unicode(random_str)
