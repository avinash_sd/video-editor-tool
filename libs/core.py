from datetime import timedelta
import os
from flask import Flask, current_app, session

from celery import Celery, Task
from flask_wtf import CSRFProtect

from models import db
from views import api_bp, main_bp


def app_before_request():
    session.permanent = True
    current_app.permanent_session_lifetime = timedelta(days=31)


def attach_static_file_cache_buster(endpoint, values):
    if endpoint == 'static':
        filename = values.get('filename', None)
        if filename:
            cache_buster = current_app.config.get('CACHE_BUSTER')
            if not cache_buster:
                try:
                    static_folder = current_app.static_folder
                    file_stat = os.stat(os.path.join(static_folder, filename))

                    cache_buster = file_stat.st_mtime
                except OSError:
                    cache_buster = ""

            values["_cb"] = cache_buster


# App Creation
def create_app(proj_env='development'):

    app = Flask("sandbox")
    load_config(app, proj_env)
    CSRFProtect(app)

    # Register blueprints
    app.register_blueprint(api_bp, url_prefix="/api")
    app.register_blueprint(main_bp)

    # for enabling 'break', 'continue' statements inside templates.
    app.jinja_env.add_extension('jinja2.ext.loopcontrols')

    app.before_request(app_before_request)
    app.url_defaults(attach_static_file_cache_buster)

    db.app = app
    db.init_app(app)

    return app


def create_celery_app(app=None):
    if not app:
        app = create_app()

    celery = Celery(app.import_name, broker=app.config['CELERY_BROKER_URL'])
    celery.conf.update(app.config)

    class ContextTask(Task):
        abstract = True

        def __call__(self, *args, **kwargs):
            with app.app_context() and app.test_request_context():
                return Task.__call__(self, *args, **kwargs)

    celery.Task = ContextTask
    return celery


def load_config(app, proj_env='development'):
    """App Configuration settings."""
    app.config.from_object('config.default')

    config_files = {
        'development': 'config.development',
        'qa': 'config.qa',
        'production': 'config.production',
    }

    proj_env = os.environ.get('PROJ_ENV', proj_env)

    if proj_env in config_files:
        app.config.from_object(config_files[proj_env])
        if os.environ.get('SECRET_KEY'):
            app.config['SECRET_KEY'] = os.environ['SECRET_KEY']

    # create folders
    for folder in ['STATIC_FOLDER', 'MEDIA_FOLDER']:
        if folder in app.config and not os.path.exists(app.config[folder]):
            os.makedirs(app.config[folder])
