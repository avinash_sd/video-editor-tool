from glob import glob
import os
import shutil
import time

from ffmpy import FFmpeg
from flask import current_app
import pyrax


from .helpers import create_directories
from models import db


class RemoteContainer(object):
    """Wrapper class for pyrax cloudfiles methods."""

    container = None
    new_folder_id = None

    def __init__(self, video_id):
        """
        Initialize the frames, videos folder.

        params:
            video_id- unicode value of the video folder name.
        """
        self.max_tries = current_app.config['MAX_RETRIES']
        self.attempts = 0
        base_folder = os.path.join(
            current_app.config['MEDIA_FOLDER'],
            video_id
        )
        self.frames_folder = os.path.join(base_folder, 'frames')
        self.videos_folder = os.path.join(base_folder, 'videos')
        self.container = self.create_container()

    def create_container(self):
        """Wrapper method for cloudfiles 'create_container' method."""
        pyrax.set_setting('identity_type', 'rackspace')
        pyrax.set_credentials(current_app.config['RACKSPACE_USERNAME'],
                              current_app.config['RACKSPACE_API_KEY'])
        self.cf = pyrax.cloudfiles
        container = self.cf.create_container(current_app.config['PROJECT_ENV'])
        metadata = {
            'Access-Control-Allow-Origin': '*'
        }
        self.cf.set_container_metadata(container, metadata)
        container.make_public()

        return container

    def delete_remote_folder(self, folder):
        """Delete remote/uploaded folder in case of exception."""
        file_list = self.container.get_object_names(prefix=folder)
        print "Folder Path: ", folder
        print "Total files to delete: ", len(file_list)
        result = self.cf.bulk_delete(self.container, file_list, async=True)
        while not result.completed:
            remaining = self.container.get_object_names(
                prefix=self.folder_prefix
            )
            print "Remaining: {}".format(len(remaining))
            time.sleep(5)
        print "Deleted!!"

    def upload_files(self, file_obj, retry_list=list()):
        """
        Wrapper method for cloudfiles 'upload_file' method.

        params:
            file_obj- SqlAlchemy object of 'File' model.
            retry_list- list containing unicde values of upload failed files.
        """
        if len(retry_list):
            files_list = retry_list
        else:
            files_list = get_file_list(
                file_obj,
                self.videos_folder,
                self.frames_folder
            )

        print 'Uploading- Folder _ID -', self.new_folder_id
        for file_name in files_list:
            object_name = os.path.join(
                current_app.config['PROJECT_ENV'],
                file_obj.unique_id,
                self.new_folder_id,
                os.path.basename(file_name)
            )
            try:
                print file_name
                upload_status = self.cf.upload_file(
                    self.container,
                    file_name,
                    obj_name=object_name,
                    headers={'Access-Control-Allow-Origin': '*'}
                )
                if file_name in retry_list:
                    retry_list.remove(file_name)
            except Exception, e:
                print e
                upload_status = False

            if not upload_status:
                retry_list.append(file_name)

        if retry_list:
            if self.attempts <= self.max_tries:
                print 'Attempting sync ', file_obj.unique_id, self.attempts
                self.attempts += 1
                return self.upload_files(file_obj, retry_list)
            else:
                return False
        retry_list = []
        print 'Upload Complete'
        return True

    def sync_folder(self, folder_path, object_prefix, attempt_count=0):
        """
        Wrapper method for cloudfiles 'sync_folder_to_container' method.

        params:
            folder_path- unicode containing the path to the folder to upload.
            object_prefix- unicode value containing remote fodler prefix.
            attempt_count- integer value for retrying the sync, if it fails.
        """
        try:
            if not os.path.exists(folder_path):
                raise IOError

            self.cf.sync_folder_to_container(
                folder_path,
                self.container,
                object_prefix=object_prefix,
                verbose=True)

            if self.cf._sync_summary["failed"] and\
                    attempt_count < current_app.config['MAX_RETRIES']:
                attempt_count += 1
                print 'Retrying - ', attempt_count
                return self.sync_folder(
                    folder_path,
                    object_prefix,
                    attempt_count
                )

            return True
        except IOError:
            print "Folder - '{}' not found".format(folder_path)
        except Exception as e:
            print e
        return False


def convert_frames_to_video(videos_folder, frames_folder, **frames_parameters):
    """
    Convert n Save frames as a single video file.

    by default converts all frames from frames_folder into a video file,
    if start_number is defined then from start_number to the last frame,
        all frames are converted to a video file,
    if both start and end_numbers are defined,
        then from start to the end all frames are converted to a video file.
    """
    options = "-r 30 -f image2 -y -i " + \
              frames_folder + \
              "/frame-%04d.jpg -movflags faststart -crf 25 -pix_fmt yuv420p"
    video_file_name = os.path.join(videos_folder, 'video-{:04d}.mp4'.format(0))

    start_number = frames_parameters.get('start')
    end_number = frames_parameters.get('end')
    frames_speed = frames_parameters.get('speed')

    # Normal/Default playback speed is 30fps while converting frames to videos.
    output_fps = '30'
    if frames_speed:
        if frames_speed == 'slow':
            output_fps = '15'
        elif frames_speed == 'fast':
            output_fps = '60'

    if start_number:
        if end_number:
            number_of_frames = end_number - start_number
            options = "-r " + output_fps + " -f image2 -y -start_number " + str(start_number) + " \
                      -i " + frames_folder + "/frame-%04d.jpg -vframes " + str(number_of_frames) + " -movflags faststart\
                      -crf 25 -pix_fmt yuv420p"
        else:
            options = "-r " + output_fps + " -f image2 -y -start_number " + str(start_number) + " \
                      -i " + frames_folder + "/frame-%04d.jpg -movflags faststart\
                      -crf 25 -pix_fmt yuv420p"

        video_file_name = os.path.join(
            videos_folder,
            'video-{:04d}.mp4'.format(start_number)
        )

    try:
        ff = FFmpeg(global_options=options, outputs={video_file_name: None})

        """
        Conversion logs are stored in '.conversion_logs' folder
        under the apps base directory.
        Since the 'FFmpeg' cmd is run thru the app,
        creating log files inside '/var/log'
        is not possible because of the permission issues.
        """
        log_folder = current_app.config.get('FFMPEG_CONVERSION_LOGS_FOLDER')
        if not os.path.exists(log_folder):
            os.makedirs(log_folder)
        log_file_name = os.path.join(
            log_folder,
            'FFmpeg_{}.log'.format(videos_folder.split('/')[-2])
        )

        with open(log_file_name, "a") as log_file:
            ff.run(stdout=log_file, stderr=log_file)
        return True
    except Exception as e:
        print 'FRAMES_CONVERSION_ERROR - ', e
        return False


def get_file_list(file_record, videos_folder, frames_folder):
    """
    Return list of files from the file record meta_data.

    params:
        file_record- SqlAlchemy object of 'File' model.
        videos_folder- string value containing path of the video folder.
        frames_folder- string value containing path of the image frames folder.
    """
    files_list = []
    for frame in file_record.meta_data['frames']:
        if frame['type'] == 'video':
            files_list.append(
                os.path.join(
                    videos_folder,
                    'video-%04d.mp4' % (frame['index'])
                )
            )
        elif frame['type'] == 'image':
            files_list.append(
                os.path.join(
                    frames_folder,
                    'frame-%04d.jpg' % (frame['index'])
                )
            )

    return files_list


def copy_files_to_upload(files_list, folder_name):
    """
    Copy image, video files to a temporary folder to upload.

    params:
        files_list- list of files to be copied.
        folder_name- string value containing temporary folder name.

    returns 'temp_folder' name after the copying all files.
    """
    try:
        temp_folder = '/tmp/{}'.format(folder_name)
        os.mkdir(temp_folder)
        for file in files_list:
            shutil.copy2(file, temp_folder)
    except Exception as e:
        print 'COPY FILES ERROR'
        raise e

    return temp_folder


def upload_to_rackspace(file_record):
    """
    Upload files from local folder to rackspace cdn.

    Get the folder with 'unique_id', create sub-folders under it as
    'frames' and 'videos'.
    Create a temporary folder with timestamp details.
    Copy the image frames, videos to the temp folder.
    Sync the temp folder to the rackspace container.
    Update the file_record with cdn ssl url and files count.

    params:
        file_record- SqlAlchemy object of 'File' model.
    """
    folder_to_upload = os.path.join(
        current_app.config['MEDIA_FOLDER'],
        file_record.unique_id
    )
    frames_folder, videos_folder = create_directories(folder_to_upload)

    for file in glob(os.path.join(folder_to_upload, '*.jpg')):
        shutil.move(file, frames_folder)

    file_record.status = u'LOGIN'
    file_record.modified_at = file_record.modified_at
    db.session.add(file_record)
    db.session.commit()

    try:
        container = RemoteContainer(file_record.unique_id)
    except Exception, e:
        print e
        file_record.status = u'LOGIN_FAILED'
        file_record.modified_at = file_record.modified_at
        db.session.add(file_record)
        db.session.commit()
        return

    file_record.status = u'SYNCING'
    file_record.modified_at = file_record.modified_at
    db.session.add(file_record)
    db.session.commit()

    print 'syncing', file_record.unique_id
    container.new_folder_id = file_record.modified_at.strftime(
        "%b%d%Y_%H_%M_%S"
    )

    # Copy files from frames, videos folder to tmp folder to upload.
    files_list = get_file_list(
        file_record,
        container.videos_folder,
        container.frames_folder
    )
    temp_folder_name = copy_files_to_upload(
        files_list,
        container.new_folder_id
    )
    object_prefix = os.path.join(
        current_app.config['PROJECT_ENV'],
        file_record.unique_id,
        container.new_folder_id
    )
    sync_status = container.sync_folder(temp_folder_name, object_prefix)

    # Legacy-code.
    # sync_status = container.upload_files(file_record)

    if not sync_status:
        container.delete_remote_folder(object_prefix)
        file_record.status = u'SYNC_FAILED'
        file_record.modified_at = file_record.modified_at
        db.session.add(file_record)
        db.session.commit()
        return
    print 'syncing done', file_record.unique_id

    try:
        cdn_ssl_url = os.path.join(
            container.container.cdn_ssl_uri,
            object_prefix
        )
        files_count = len(next(os.walk(frames_folder))[2])
        file_record.status = u'UPLOAD_SUCCESS'
        if file_record.meta_data:
            file_record.meta_data['cdn_url'] = unicode(cdn_ssl_url)
            file_record.meta_data['count'] = files_count
        else:
            file_record.meta_data = dict(
                cdn_url=unicode(cdn_ssl_url),
                count=files_count,
                frames=[]
            )
        file_record.modified_at = file_record.modified_at
        db.session.add(file_record)
        db.session.commit()
    except Exception, e:
        container.delete_remote_folder(object_prefix)
        print e
        file_record.status = u'EXPORT_FAILED'
        file_record.modified_at = file_record.modified_at
        db.session.add(file_record)
        db.session.commit()

    # Legacy-code.
    # cleanup_assets(file_record)