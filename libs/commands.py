from datetime import datetime
from dateutil.relativedelta import relativedelta

from flask_script import Command, Option

from models import File
from .helpers import cleanup_assets
from .utils import upload_to_rackspace


class AddCacheBuster(Command):
    def run(self):
        """Function to add chache buster."""
        try:
            config_file = open("config/default.py", 'r+')
            for line in config_file:
                if 'CACHE_BUSTER' in line:
                    config_file.seek(-len(line), 1)
                    cache_bust_line = "CACHE_BUSTER = '" +\
                        datetime.strftime(
                            datetime.now(),
                            '%Y%m%d%H%M'
                        ) + "'"
                    config_file.write(cache_bust_line.ljust(len(line)))
                    break
            else:
                config_file.write(
                    '\nCACHE_BUSTER = ' + "'" +
                    datetime.strftime(
                        datetime.now(),
                        '%Y%m%d%H%M'
                    ) + "'")

            config_file.flush()
            config_file.close()

        except Exception as e:
            print e


class CleanFiles(Command):
    """
    Command to remove dedundant/deleted files and its assets.

    options:
        '-date' date till all files has to be removed.
        default value is 6 months
    """

    option_list = (
        Option('-till_date', dest='till_date'),
    )

    def run(self, till_date):
        if till_date:
            try:
                till_date = datetime.strptime("DD-MM-YYYY")
            except Exception:
                print "Invalid date. Format should be DD-MM-YYYY"
                return

        else:
            till_date = datetime.utcnow() - relativedelta(months=1)

        files = File.query.filter(
            File.is_deleted.__eq__(True),
            File.modified_at < till_date
        ).all()

        for _file in files:
            cleanup_assets(_file)


class SyncFolder(Command):
    """
    Command to Sync local folder to Rackspace cdn.

    options:
        '-file_id' unicode value of 'File' record's unique_id.
    """

    option_list = (
        Option('-file_id', dest='file_id'),
    )

    def run(self, file_id):
        """
        Upload local folder contents to Rackspace.

        params:
            file_id- unicode value of 'File' record's unique_id.
        """
        file_record = File.query.filter(
            File.is_deleted.__eq__(False),
            File.unique_id == unicode(file_id)
        ).first_or_404()
        upload_to_rackspace(file_record)
