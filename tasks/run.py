import os
import shutil

import av
from flask import current_app
from PIL import Image, ImageChops, ImageStat

from .factory import celery
from libs import (
    cleanup_assets,
    convert_frames_to_video,
    create_directories,
    upload_to_rackspace
)
from models import File, db


@celery.task
def create_video_files(video_details):
    """Fetch the file record, read meta_data, create frames, video files."""
    file_record = File.query.filter(
        (File.is_deleted.__eq__(False)) &
        (File.unique_id == video_details['video_id'])
    ).first_or_404()

    try:
        if file_record.status in [u'LOGIN_FAILED',
                                  u'SYNC_FAILED',
                                  u'EXPORT_FAILED']:
            return upload_to_rackspace(file_record)

        file_record.status = u'OPTIMIZING'
        db.session.add(file_record)
        db.session.commit()

        asset_folder = os.path.join(
            current_app.config['MEDIA_FOLDER'],
            file_record.unique_id)
        frames_folder, videos_folder = create_directories(asset_folder)

        # Clean the video_folder contents.
        shutil.rmtree(videos_folder, ignore_errors=True)
        os.makedirs(videos_folder)

        fr_meta_data = file_record.meta_data['frames']
        new_frames_data = list()
        is_converted = False
        total_frames = len(os.listdir(frames_folder))

        if not fr_meta_data:
            # If there are no hotspots,
            # then convert all frames into a video file.
            is_converted = convert_frames_to_video(
                videos_folder,
                frames_folder)
            if not is_converted:
                raise Exception('FRAMES_CONVERSION_ERROR')
            new_frames_data.append({'index': 0, "type": 'video'})
        else:
            """
                filter meta_data to only image content, sort the meta_data in
                ascending order w.r.t index.
            """
            fr_meta_data = [f_data for f_data in fr_meta_data
                            if f_data.get('type') == 'image']
            fr_meta_data = sorted(fr_meta_data, key=lambda x: x['index'])
            fr_index = 1
            start_frame = None
            end_frame = None
            for frame_data in fr_meta_data:
                new_frames_data.append(frame_data)
                start_frame = frame_data.get('index')
                end_frame = (fr_meta_data[fr_index].get('index'))\
                    if fr_index < len(fr_meta_data) else None

                """
                    If the start and end frames are consecutive,
                    then skip video conversion.
                """
                if start_frame is not None and end_frame is not None:
                    if end_frame == (start_frame + 1):
                        continue

                # 'is not None' is used, since value of frames can also be 0.
                if start_frame is not None:
                    start_frame = start_frame + 1
                if end_frame is not None:
                    end_frame = end_frame - 1

                if start_frame and start_frame < total_frames:
                    if end_frame:
                        if end_frame < start_frame:
                            raise Exception('FRAMES_CONVERSION_ERROR')
                        frames_speed = fr_meta_data[
                            fr_index].get('frames_speed')
                        is_converted = convert_frames_to_video(
                            videos_folder, frames_folder,
                            **{'start': start_frame, 'end': end_frame,
                               'speed': frames_speed}
                        )
                    else:
                        is_converted = convert_frames_to_video(
                            videos_folder, frames_folder,
                            **{'start': start_frame}
                        )

                    if not is_converted:
                        raise Exception('FRAMES_CONVERSION_ERROR')
                    new_frames_data.append({
                        "index": start_frame,
                        "type": 'video'
                    })
                fr_index = fr_index + 1

        # Sort w.r.t index value.
        new_frames_data = sorted(new_frames_data, key=lambda x: x['index'])
        file_record.meta_data['frames'] = new_frames_data
        file_record.status = u'READY'
        db.session.add(file_record)
        db.session.commit()
        upload_to_rackspace(file_record)

    except Exception, e:
        print e
        file_record.status = u'OPTIMIZING_FAILED'
        db.session.add(file_record)
        db.session.commit()


@celery.task
def extract_frames(video_details):
    """
    Convert a video file into image frames.

    Get a video file, convert it into image frames,
    filter out duplicate images.
    Save the filtered image frames in the 'frames' folder.

    params:
        video_details- dict containing 'id', 'file_path' keys.
    """
    file_record = File.query.filter(
        (File.is_deleted.__eq__(False)) &
        (File.id == video_details['id'])
    ).first_or_404()
    try:
        print 'Extracting Frames- ', file_record.unique_id
        folder_path = os.path.join(
            current_app.config['MEDIA_FOLDER'],
            file_record.unique_id
        )
        frames_dir, videos_dir = create_directories(folder_path)

        image_format = current_app.config.get('IMAGE_FORMAT')
        container = av.open(str(video_details['file_path']))
        video = next(s for s in container.streams if s.type == b'video')
        for packet in container.demux(video):
            for frame in packet.decode():
                frame.to_image().save(
                    frames_dir + '/frame-%04d.%s' % (
                        frame.index, image_format),
                    quality=current_app.config['IMAGE_QUALITY'])

        del_index_list = []
        for i in xrange(frame.index):
            img1 = Image.open(
                frames_dir + '/frame-%04d.%s' % (i, image_format))
            img2 = Image.open(
                frames_dir + '/frame-%04d.%s' % ((i + 1), image_format))

            r, g, b = ImageStat.Stat(ImageChops.difference(img1, img2)).rms
            is_equal = (r < 0.20 and g < 0.20 and b < 0.20)

            if (is_equal):
                del_index_list.append(i)

        for index in del_index_list:
            os.remove(frames_dir + '/frame-%04d.%s' % (index, image_format))

        frame_count = 0
        for i in xrange(frame.index + 1):
            if i in del_index_list:
                continue

            if i != frame_count:
                old_fname = frames_dir + '/frame-%04d.%s' % (i, image_format)
                new_fname = frames_dir + '/frame-%04d.%s' % (
                    frame_count, image_format)
                os.rename(old_fname, new_fname)

                frame_count += 1

        file_record.status = u'READY'
        file_record.meta_data = dict(
            cdn_url=unicode(
                '/static/media/' + file_record.unique_id + '/frames'),
            count=len(next(os.walk(frames_dir))[2]),
            frames=[]
        )
        db.session.add(file_record)
        db.session.commit()
        print 'Extraction Done - ', file_record.unique_id

    except Exception, e:
        print e
        file_record.status = u'PROCESSING_FAILED'
        db.session.add(file_record)
        db.session.commit()
        cleanup_assets(file_record)
        return {'status': 'FAILED', 'message': e.message}
