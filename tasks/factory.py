from flask import current_app
from libs import create_celery_app

celery = create_celery_app(app=current_app)
