import React from "react";
import ReactDOM from "react-dom";

import { Edit } from "./components/edit";
import "bootstrap/dist/css/bootstrap.min.css";
import "jquery-ui/themes/base/draggable.css";
import "jquery-ui/themes/base/resizable.css";
import "jquery-ui/themes/base/selectable.css";
import "@fortawesome/fontawesome-free/css/all.css";
import "../css/edit.css";

ReactDOM.render(<Edit />, document.getElementById("root"));
