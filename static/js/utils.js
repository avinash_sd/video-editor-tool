import _ from "underscore";

export function formatNumber(number, size = 1) {
  let text = number + "";
  while (text.length < size) text = "0" + text;
  return text;
}

export function getAttributes(frames, property) {
  return _.map(
    _.filter(frames, frame => {
      return !_.isEmpty(frame[property]);
    }),
    _attrWithIndex => {
      return _.extend(_attrWithIndex[property], {
        index: _attrWithIndex.index
      });
    }
  );
}

export default { formatNumber, getAttributes };
