import React, { Component } from "react";

class SearchVideos extends Component {
  constructor() {
    super();
    this.state = {
      searchBy: "name",
      text: ""
    };
  }

  searchBy = e => {
    this.setState(
      {
        searchBy: e.target.value
      },
      this.search
    );
  };

  handleTextChange = e => {
    const { value } = e.target;
    this.setState(
      {
        text: value
      },
      this.search
    );
  };

  search = () => {
    this.props.searchVideos({
      searchBy: this.state.searchBy,
      text: this.state.text
    });
  };

  render() {
    return (
      <div className="input-group mb-3">
        <input
          type="text"
          className="form-control"
          onChange={this.handleTextChange}
          value={this.state.text}
        />
        <select
          className="custom-select mb-3 text-uppercase"
          onChange={this.searchBy}
        >
          <option value="name">name</option>
          <option value="status">status</option>
        </select>
      </div>
    );
  }
}

export default SearchVideos;
