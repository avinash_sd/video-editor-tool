import React, { Component } from "react";

function UploadButton(props) {
  return (
    <div className="p-0 flex-fill align-self-center text-right">
      <button
        className="btn btn-primary text-uppercase"
        disabled={!props.data.file || props.data.isSubmitted}
      >
        <span>
          {props.data.isSubmitted ? props.data.progress + ` %` : "upload"}
        </span>
      </button>
    </div>
  );
}

class UploadVideo extends Component {
  constructor() {
    super();
    this.fileInput = React.createRef();
    this.initialState = {
      file: undefined,
      progress: 0,
      isSubmitted: false
    };

    this.state = this.initialState;
  }

  handleFileChange = event => {
    this.setState({
      file: this.fileInput.current.files[0]
    });
  };

  handleSubmit = event => {
    event.preventDefault();
    if (!this.state.file) return false;
    this.setState({
      isSubmitted: true
    });
    const root = this;
    const xhr = new XMLHttpRequest();
    (xhr.upload || xhr).addEventListener("progress", e => {
      const done = e.position || e.loaded;
      const total = e.totalSize || e.total;
      const progress = Math.round((done / total) * 100);
      root.setState({
        progress: progress
      });
    });

    xhr.addEventListener("load", e => {
      root.setState(root.initialState);
      root.fileInput.current.value = "";
      this.props.loadVideos();
    });

    xhr.addEventListener("error", (msg, status, jqXHR) => {
      console.info(msg, status, jqXHR);
      root.setState(root.initialState);
      root.fileInput.current.value = "";
    });
    xhr.open("post", "/api/video/upload", true);
    xhr.setRequestHeader(
      "X-CSRFToken",
      document.querySelector('meta[name="csrf-token"]').content
    );

    var data = new FormData();
    data.append("video", this.state.file);
    xhr.send(data);
  };

  render() {
    return (
      <ul className="list-group mb-3">
        <li className="list-group-item p-2">
          <form
            className="d-flex flex-fill"
            action="/"
            encType="multipart/form-data"
            onSubmit={this.handleSubmit}
          >
            <div className="p-0 flex-fill align-self-center">
              <input
                type="file"
                name="video"
                accept=".mp4"
                onChange={this.handleFileChange}
                ref={this.fileInput}
              />
            </div>
            <UploadButton data={this.state} />
          </form>
        </li>
      </ul>
    );
  }
}

export default UploadVideo;
