import React from "react";

import Pagination from "./Pagination";
import SearchVideos from "./SearchVideos";
import Video from "./Video";

function VideoListEmpty() {
  return (
    <li className="list-group-item">
      <div className="text-center">
        <span className="h5 text-capitalize">no videos available!!</span>
      </div>
    </li>
  );
}

function VideoList(props) {
  if (!props.videoList.length) {
    return (
      <ul className="list-group mb-3">
        <VideoListEmpty />
      </ul>
    );
  }

  const videosList = props.videoList.map(video => {
    return (
      <Video key={video.unique_id} data={video} loadVideos={props.loadVideos} />
    );
  });
  return <ul className="list-group mb-3 videoList">{videosList}</ul>;
}

export default VideoList;
