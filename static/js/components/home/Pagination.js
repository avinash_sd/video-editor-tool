import React, { Component } from "react";
import _ from "underscore";

class Pagination extends Component {
  loadPage = page => {
    this.props.loadVideos({ page: page });
  };

  render() {
    const { currentPage, totalPages } = this.props;
    const PAGE_LIMIT = 10;
    let pageEnd = Math.ceil(currentPage / PAGE_LIMIT) * PAGE_LIMIT;
    pageEnd = Math.min(pageEnd, totalPages);
    const substractFactor = currentPage % PAGE_LIMIT || PAGE_LIMIT;
    const pageStart = currentPage - substractFactor + 1;

    const pages = _.map(_.range(pageStart, pageEnd + 1), pageIdx => {
      return (
        <li
          className={pageIdx === currentPage ? "page-item active" : "page-item"}
          key={pageIdx}
          onClick={() => pageIdx !== currentPage && this.loadPage(pageIdx)}
        >
          <a className="page-link">{pageIdx}</a>
        </li>
      );
    });

    return (
      <ul className="pagination justify-content-center">
        <li
          className={
            currentPage <= PAGE_LIMIT ? "page-item disabled" : "page-item"
          }
          onClick={() =>
            currentPage > PAGE_LIMIT &&
            this.loadPage(Math.min(currentPage - PAGE_LIMIT), 1)
          }
        >
          <a className="page-link">«</a>
        </li>
        <li
          className={currentPage == 1 ? "page-item disabled" : "page-item"}
          onClick={() => currentPage > 1 && this.loadPage(currentPage - 1)}
        >
          <a className="page-link">‹</a>
        </li>
        {pages}
        <li
          className={
            currentPage >= totalPages ? "page-item disabled" : "page-item"
          }
          onClick={() =>
            currentPage < totalPages && this.loadPage(currentPage + 1)
          }
        >
          <a className="page-link">›</a>
        </li>
        <li
          className={
            currentPage + PAGE_LIMIT > totalPages
              ? "page-item disabled"
              : "page-item"
          }
          onClick={() =>
            currentPage + PAGE_LIMIT <= totalPages &&
            this.loadPage(Math.min(currentPage + PAGE_LIMIT), totalPages)
          }
        >
          <a className="page-link">»</a>
        </li>
      </ul>
    );
  }
}

export default Pagination;
