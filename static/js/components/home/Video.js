import React, { Component } from "react";
import _ from "underscore";
import $ from "jquery";
import { Button, Modal } from "react-bootstrap";

class Video extends Component {
  constructor() {
    super();
    this.state = {
      status: "",
      videoId: "",
      confirmDeleteAction: false,
      videoName: ""
    };
  }

  componentDidMount() {
    this.setState({
      status: this.props.data.status,
      videoId: this.props.data.unique_id,
      videoName: this.props.data.name
    });
  }

  checkVideoStatus = () => {
    fetch("/api/videos/" + this.state.videoId + "/status")
      .then(response => response.json())
      .then(data => {
        this.setState({
          status: data.status
        });
      });
  };

  closeConfirmDelete = () => {
    this.setState({
      confirmDeleteAction: false
    });
  };

  confirmDelete = () => {
    this.setState({
      confirmDeleteAction: true
    });
  };

  deleteVideo = () => {
    fetch("/api/videos/" + this.state.videoId, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        "X-CSRFToken": $("meta[name=csrf-token]").attr("content")
      }
    })
      .then(response => response.json())
      .then(data => {
        this.props.loadVideos();
      });
  };

  uploadToCDN = () => {
    fetch("/api/videos/" + this.state.videoId + "/upload-to-cdn/", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "X-CSRFToken": $("meta[name=csrf-token]").attr("content")
      }
    })
      .then(response => response.json())
      .then(data => {
        this.setState({
          status: data.status
        });
      });
  };

  render() {
    const processCompleteList = [
      "EXPORT_FAILED",
      "LOGIN_FAILED",
      "PROCESSING_FAILED",
      "READY",
      "SYNC_FAILED",
      "UPLOAD_SUCCESS"
    ];
    const processSuccessList = ["READY"];
    const processProgressList = ["OPTIMIZING"];
    const processErrorList = ["OPTIMIZING_FAILED", "PROCESSING_FAILED"];
    const cdnSuccessStatusList = ["UPLOAD_SUCCESS"];
    const cdnProgressStatusList = [
      "LOGIN",
      "PROCESSING",
      "SYNCING",
      "UPLOADING"
    ];
    const cdnFailStatusList = ["EXPORT_FAILED", "LOGIN_FAILED", "SYNC_FAILED"];
    const dateOptions = {
      weekday: "long",
      year: "numeric",
      month: "short",
      day: "numeric",
      hour: "numeric"
    };
    const { status } = this.state;
    const { created_at, name, unique_id } = this.props.data;
    const date = new Date(created_at);
    const canEdit = _.contains(
      _.union(
        processSuccessList,
        cdnFailStatusList,
        cdnSuccessStatusList,
        processErrorList
      ),
      status
    );
    const isProcessing = _.contains(
      _.union(processProgressList, cdnProgressStatusList),
      status
    );
    const canDownload = _.contains(cdnSuccessStatusList, status);
    const canUploadToCDN = _.contains(
      _.union(processSuccessList, cdnFailStatusList),
      status
    );
    return (
      <React.Fragment>
        <li className="list-group-item">
          <div className="d-flex row">
            <div className="col-6 flex-fill align-self-center text-left">
              <div>
                <h6 className="my-0" title={name}>
                  {name}
                </h6>
                <small className="text-muted">
                  Created on {date.toLocaleDateString("en", dateOptions)}
                </small>
              </div>
            </div>
            <div className="col-3 flex-fill align-self-center text-center">
              <span className="text-uppercase">{status.replace("_", " ")}</span>
              {isProcessing ? (
                <button
                  className="btn m-1 round"
                  onClick={this.checkVideoStatus}
                >
                  <i className="fas fa-sync-alt" />
                </button>
              ) : null}
            </div>
            <div className="col-3 flex-fill align-self-center text-right">
              {canEdit ? (
                <a
                  className="btn btn-info mr-1"
                  data-toggle="tooltip"
                  title="Edit"
                  target="_blank"
                  href={"/edit/" + unique_id}
                >
                  <i className="fas fa-edit" />
                </a>
              ) : null}
              {canDownload ? (
                <a
                  className="btn btn-primary mr-1"
                  data-toggle="tooltip"
                  title="Download json file"
                  href={`/api/videos/${unique_id}?download=1`}
                >
                  <i className="fas fa-download" />
                </a>
              ) : null}
              {canUploadToCDN ? (
                <button
                  className="btn btn-success mr-1"
                  data-toggle="tooltip"
                  title="Upload to CDN"
                  onClick={this.uploadToCDN}
                >
                  <i className="fas fa-upload" />
                </button>
              ) : null}
              <button
                className="btn btn-danger"
                title="Delete"
                onClick={this.confirmDelete}
              >
                <i className="fas fa-trash" />
              </button>
            </div>
          </div>
        </li>
        <Modal show={this.state.confirmDeleteAction} onHide={this.closeNotes}>
          <Modal.Body>
            Are you sure you want to delete
            <br />
            <strong>{this.state.videoName}</strong>?
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.closeConfirmDelete}>
              No
            </Button>
            <Button variant="primary" onClick={this.deleteVideo}>
              Yes
            </Button>
          </Modal.Footer>
        </Modal>
      </React.Fragment>
    );
  }
}
export default Video;
