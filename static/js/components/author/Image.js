import React, { Component } from "react";
import _ from "underscore";
import { formatNumber } from "../../utils";
import $ from "jquery";
import "jquery-ui/ui/widgets/draggable";
import "jquery-ui/ui/widgets/resizable";
import "jquery-ui/ui/widgets/selectable";

class Image extends Component {
  deleteHotspot = e => {
    if (confirm("Do you want delete this hotspot?")) {
      this.props.deleteAnnotation();
    }
  };

  componentDidUpdate() {
    _.each(this.refs, ref => {
      if (!this.props.drawHotspot) {
        $(ref).selectable("instance") && $(ref).selectable("destroy");
      }
      if (!this.props.editHotspot) {
        const elem = $(ref).find(".highlight");
        elem.draggable("instance") && elem.draggable("destroy");
        elem.resizable("instance") && elem.resizable("destroy");
      }
    });
    if (this.props.index !== this.props.currentImage) return false;
    const root = this;
    if (this.props.drawHotspot) {
      let Xstart, Ystart;
      $(this.refs.active).selectable({
        start: function(e) {
          //get the mouse position on start
          Xstart = e.pageX;
          Ystart = e.pageY;
        },
        stop: function(e) {
          const Xend = e.pageX,
            Yend = e.pageY;
          const viewPort = root.refs.active.getBoundingClientRect();
          const scrollTop =
            window.pageYOffset ||
            (
              document.documentElement ||
              document.body.parentNode ||
              document.body
            ).scrollTop;
          let left = Xstart < Xend ? Xstart : Xend;
          let top = Ystart < Yend ? Ystart : Yend;
          let hotspot = {
            top: Math.abs(top - viewPort.top - scrollTop - 1),
            left: Math.abs(left - viewPort.left + 1),
            width: Math.abs(Xstart - Xend),
            height: Math.abs(Ystart - Yend)
          };
          hotspot = root.parseDimensions(hotspot);
          root.props.updateAnnotations(hotspot);
        }
      });
    }
    if (this.props.editHotspot) {
      const annotation = $(this.refs.active).find(".highlight");
      annotation.length &&
        !annotation.draggable("instance") &&
        annotation.draggable({
          containment: $(this.refs.active),
          stop: function(e) {
            root.updateAnnotation(e.target);
          }
        });
      annotation.length &&
        !annotation.resizable("instance") &&
        annotation.resizable({
          containment: $(this.refs.active),
          handles: "all",
          stop: function(e) {
            root.updateAnnotation(e.target);
          }
        });
    }
  }

  updateAnnotation = elem => {
    let dimensions = this.getElementStyles(elem);
    dimensions = this.parseDimensions(dimensions);
    this.props.updateAnnotations(dimensions);
  };

  getElementStyles = elem => {
    return {
      top: elem.offsetTop,
      left: elem.offsetLeft,
      width: elem.offsetWidth,
      height: elem.offsetHeight
    };
  };

  parseDimensions = hotspot => {
    const { clientWidth, clientHeight } = this.refs.active;

    return {
      top: parseFloat((hotspot.top / clientHeight) * 100).toFixed(2),
      height: parseFloat((hotspot.height / clientHeight) * 100).toFixed(2),
      left: parseFloat((hotspot.left / clientWidth) * 100).toFixed(2),
      width: parseFloat((hotspot.width / clientWidth) * 100).toFixed(2)
    };
  };

  render() {
    const {
      annotation,
      currentImage,
      drawHotspot,
      index,
      imageRoot,
      togglePlay
    } = this.props;
    let style;
    if (!_.isEmpty(annotation)) {
      style = {
        top: annotation.top + "%",
        left: annotation.left + "%",
        width: annotation.width + "%",
        height: annotation.height + "%"
      };
    }
    return (
      <li
        className={index === currentImage ? "border active" : "border"}
        ref={index === currentImage ? "active" : index}
      >
        {style ? (
          <div
            className="highlight"
            style={style}
            onClick={!drawHotspot ? togglePlay : undefined}
          >
            <div className="delete" onClick={this.deleteHotspot} />
          </div>
        ) : null}
        <img
          src={imageRoot + "/frame-" + formatNumber(index, 4) + ".jpg"}
          className="w-100"
        />
      </li>
    );
  }
}

export default Image;
