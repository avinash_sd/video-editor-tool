import React, { Component } from "react";
import $ from "jquery";
import "jquery-ui/ui/widgets/slider";

class Controls extends Component {
  componentDidUpdate() {
    this.initSeekBar();
    $(this.refs.seekbar).slider("value", this.props.currentImage);
  }

  initSeekBar() {
    const root = this;
    $(this.refs.seekbar).slider({
      max: this.props.totalImages - 1,
      stop: function(event, ui) {
        var index = ui.value < 0 ? 0 : ui.value;
        root.props.gotoImage(index);
      }
    });
  }

  render() {
    return (
      <div className="row controls ml-2 mr-2 mt-4 mb-4">
        <div ref="seekbar" className="col-12" />
      </div>
    );
  }
}

export default Controls;
