import React, { Component } from "react";

class Annotations extends Component {
  render() {
    const { annotations } = this.props;
    const classList = "text-center p-0 m-1 border";
    const classListActive = "text-center p-0 m-1 border active";
    return (
      <div className="col-1 float-left bookmarks pl-0 pr-0">
        <ul className="list-unstyled">
          {annotations &&
            annotations.map(bookmark => (
              <li
                key={bookmark.index}
                className={
                  bookmark.index === this.props.currentImage
                    ? classListActive
                    : classList
                }
                onClick={() => this.props.jumpToAnnotation(bookmark.index)}
              >
                {bookmark.index}
              </li>
            ))}
        </ul>
      </div>
    );
  }
}

export default Annotations;
