import React, { Component } from "react";
import _ from "underscore";
import { Button, Form, ListGroup, Modal } from "react-bootstrap";
import { DEFAULT_FRAME_SPEED } from "../edit";

class Options extends Component {
  constructor() {
    super();

    this.state = {
      showNotes: false,
      showBookmark: false,
      showSpeedSettings: false,
      showChooseLanguage: false,
      notes: "",
      bookmarkTitle: "",
      bookmarkDescription: "",
      frameSpeed: DEFAULT_FRAME_SPEED
    };
  }

  gotoNext = () => {
    this.props.gotoImage(this.props.currentImage + 1);
  };

  gotoPrevious = () => {
    this.props.gotoImage(this.props.currentImage - 1);
  };

  openNotes = e => {
    let attrs = {
      showNotes: true,
      notes: ""
    };

    let notes = this.props.notes.find(
      _bm => _bm.index === this.props.currentImage
    );

    const selectedLocale = this.props.selectedLanguage.id;
    if (notes && notes[selectedLocale]) {
      attrs.notes = notes[selectedLocale];
    }
    this.setState(attrs);
  };

  saveNotes = () => {
    this.props.updateNotes({
      text: this.state.notes
    });
    this.setState({
      showNotes: false
    });
  };

  closeNotes = e => {
    this.setState({
      showNotes: false
    });
  };

  openBookmark = e => {
    let attrs = {
      showBookmark: true,
      bookmarkTitle: "",
      bookmarkDescription: ""
    };

    let bookmark = this.props.bookmarks.find(
      _bm => _bm.index === this.props.currentImage
    );

    const selectedLocale = this.props.selectedLanguage.id;
    if (bookmark && bookmark.title && bookmark.title[selectedLocale]) {
      attrs.bookmarkTitle = bookmark.title[selectedLocale];
    }
    if (
      bookmark &&
      bookmark.description &&
      bookmark.description[selectedLocale]
    ) {
      attrs.bookmarkDescription = bookmark.description[selectedLocale];
    }
    this.setState(attrs);
  };

  handleChange = () => {
    const { name, type, value, checked } = event.target;
    this.setState({
      [name]: type === "checkbox" ? checked : value
    });
  };

  saveBookmark = () => {
    this.props.updateBookmarks({
      title: this.state.bookmarkTitle,
      description: this.state.bookmarkDescription
    });
    this.setState({
      showBookmark: false
    });
  };

  closeBookmark = () => {
    this.setState({
      showBookmark: false
    });
  };

  openSpeedSettings = e => {
    const { speedSettings, currentImage } = this.props;
    const setting = _.find(speedSettings, _s => _s.index === currentImage);
    const frameSpeed = setting ? setting.frameSpeed : DEFAULT_FRAME_SPEED;
    this.setState({
      showSpeedSettings: true,
      frameSpeed: frameSpeed
    });
  };

  closeSpeedSettings = e => {
    this.setState({
      showSpeedSettings: false
    });
  };

  saveSpeedSettings = () => {
    this.props.updateFrameSpeed({
      frameSpeed: this.state.frameSpeed
    });
    this.setState({
      showSpeedSettings: false
    });
  };

  openChooseLanguage = e => {
    this.setState({
      showChooseLanguage: true
    });
  };

  closeChooseLanguage = e => {
    this.setState({
      showChooseLanguage: false
    });
  };

  switchLanguage = language_id => {
    document.cookie = `locale_id=${language_id};path=/`;
    window.location.reload();
  };

  render() {
    const languageList = !this.props.languages
      ? []
      : this.props.languages.map((language, idx) => (
          <ListGroup.Item
            action
            as={"li"}
            className={language.selected ? "selected" : null}
            key={idx}
            onClick={() =>
              !language.selected && this.switchLanguage(language.id)
            }
          >
            {language.name}
          </ListGroup.Item>
        ));
    const classList = "text-center h3 p-2 m-1 border";
    const disabledClass = classList + " disabled";
    const classListActive = classList + " active";
    const hasAnnotation = _.find(
      this.props.annotations,
      _a => _a.index === this.props.currentImage
    );
    return (
      <React.Fragment>
        <div className="col-1 float-right right-panel pl-0 pr-0">
          <ul className="list-unstyled">
            <li
              className={
                !hasAnnotation
                  ? disabledClass
                  : this.state.showNotes
                  ? classListActive
                  : classList
              }
              title="Notes"
              onClick={hasAnnotation && this.openNotes}
            >
              <i className="far fa-file-alt" />
            </li>
            <li
              className={
                !hasAnnotation
                  ? disabledClass
                  : this.state.showBookmark
                  ? classListActive
                  : classList
              }
              title="Bookmarks"
              onClick={hasAnnotation && this.openBookmark}
            >
              <i className="far fa-bookmark" />
            </li>
            <li
              className={this.props.editHotspot ? classListActive : classList}
              title="Annotations(square)"
              onClick={this.props.toggleDrawHotspot}
            >
              <i className="fas fa-vector-square" />
            </li>
            <li
              className={
                !hasAnnotation
                  ? disabledClass
                  : this.state.showSpeedSettings
                  ? classListActive
                  : classList
              }
              title="Frame Speed"
              onClick={hasAnnotation && this.openSpeedSettings}
            >
              <i className="fas fa-tachometer-alt" />
            </li>
            <li
              className={classList}
              title="Save Changes"
              onClick={this.props.saveChanges}
            >
              <i className="fas fa-save" />
            </li>
            <li
              className={classList}
              title="Next Image"
              onClick={this.gotoNext}
            >
              <i className="fas fa-chevron-right" />
            </li>
            <li
              className={classList}
              title="Previous Image"
              onClick={this.gotoPrevious}
            >
              <i className="fas fa-chevron-left" />
            </li>
            <li
              className={
                this.state.showChooseLanguage ? classListActive : classList
              }
              title="Language"
              onClick={this.openChooseLanguage}
            >
              <i className="fas fa-globe-americas" />
              <div style={{ fontSize: "10px" }}>
                {this.props.selectedLanguage &&
                  this.props.selectedLanguage.name}
              </div>
            </li>
            <li
              className={classList}
              title="Apply changes and play"
              onClick={this.props.togglePlay}
            >
              <i
                className={
                  this.props.isPlaying ? "fas fa-pause" : "fas fa-play"
                }
              />
            </li>
            <li
              className="text-center p-2 m-1 border"
              style={{ fontSize: "15px" }}
            >
              {this.props.currentImage}/{this.props.totalImages}
            </li>
          </ul>
        </div>
        <Modal show={this.state.showNotes} onHide={this.closeNotes}>
          <Modal.Header closeButton>
            <Modal.Title>Add Notes</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <Form.Group controlId="notes.ControlTextarea">
                <Form.Control
                  as="textarea"
                  rows="5"
                  name="notes"
                  onChange={this.handleChange}
                  value={this.state.notes}
                  placeholder="Write something.."
                />
              </Form.Group>
            </Form>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.closeNotes}>
              Close
            </Button>
            <Button variant="primary" onClick={this.saveNotes}>
              Save Changes
            </Button>
          </Modal.Footer>
        </Modal>
        <Modal show={this.state.showBookmark} onHide={this.closeBookmark}>
          <Modal.Header closeButton>
            <Modal.Title>Add Bookmark</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <Form.Control
                type="text"
                placeholder="Write something.."
                className="mb-2"
                name="bookmarkTitle"
                onChange={this.handleChange}
                value={this.state.bookmarkTitle}
              />
              <Form.Group controlId="bookmark.ControlTextarea">
                <Form.Control
                  as="textarea"
                  rows="5"
                  placeholder="Write something.."
                  name="bookmarkDescription"
                  onChange={this.handleChange}
                  value={this.state.bookmarkDescription}
                />
              </Form.Group>
            </Form>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.closeBookmark}>
              Close
            </Button>
            <Button variant="primary" onClick={this.saveBookmark}>
              Save Changes
            </Button>
          </Modal.Footer>
        </Modal>
        <Modal
          show={this.state.showSpeedSettings}
          onHide={this.closeSpeedSettings}
        >
          <Modal.Header closeButton>
            <Modal.Title>Select Frame Transition Speed</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <Form.Check type="radio">
                <Form.Check.Input
                  type="radio"
                  name="frameSpeed"
                  value="slow"
                  checked={this.state.frameSpeed == "slow"}
                  onChange={this.handleChange}
                />
                <Form.Check.Label>Slow(15 FPS)</Form.Check.Label>
              </Form.Check>
              <Form.Check type="radio">
                <Form.Check.Input
                  type="radio"
                  name="frameSpeed"
                  value="normal"
                  checked={this.state.frameSpeed == "normal"}
                  onChange={this.handleChange}
                />
                <Form.Check.Label>Normal(33 FPS)</Form.Check.Label>
              </Form.Check>
              <Form.Check type="radio">
                <Form.Check.Input
                  type="radio"
                  name="frameSpeed"
                  value="fast"
                  checked={this.state.frameSpeed == "fast"}
                  onChange={this.handleChange}
                />
                <Form.Check.Label>Fast(60 FPS)</Form.Check.Label>
              </Form.Check>
            </Form>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.closeSpeedSettings}>
              Close
            </Button>
            <Button variant="primary" onClick={this.saveSpeedSettings}>
              Save Changes
            </Button>
          </Modal.Footer>
        </Modal>
        <Modal
          show={this.state.showChooseLanguage}
          onHide={this.closeChooseLanguage}
          className="chooseLanguage"
        >
          <Modal.Header closeButton>
            <Modal.Title>Choose Language</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <ListGroup as={"ul"}>{languageList}</ListGroup>
          </Modal.Body>
        </Modal>
      </React.Fragment>
    );
  }
}

export default Options;
