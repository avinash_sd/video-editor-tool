import React, { Component } from "react";
import Image from "./Image";

class ImageList extends Component {
  render() {
    const {
      annotations,
      currentImage,
      drawHotspot,
      editHotspot,
      imageIndices,
      imageRoot,
      deleteAnnotation,
      togglePlay,
      updateAnnotations
    } = this.props;
    const imageList = imageIndices.map(index => {
      const annotation = annotations.find(at => at.index == index);
      return (
        <Image
          key={index}
          annotation={annotation}
          currentImage={currentImage}
          drawHotspot={drawHotspot}
          editHotspot={editHotspot}
          index={index}
          imageRoot={imageRoot}
          drawHotspot={drawHotspot}
          deleteAnnotation={deleteAnnotation}
          togglePlay={togglePlay}
          updateAnnotations={updateAnnotations}
        />
      );
    });
    return (
      <div className="player row border-bottom">
        <ul className="list-unstyled mt-2 col-12">{imageList}</ul>
      </div>
    );
  }
}

export default ImageList;
