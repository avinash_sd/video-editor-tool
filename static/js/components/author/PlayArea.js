import React, { Component } from "react";

import Controls from "./Controls";
import ImageList from "./imageList";

class PlayArea extends Component {
  render() {
    const {
      annotations,
      currentImage,
      drawHotspot,
      editHotspot,
      imageIndices,
      imageRoot,
      totalImages,
      deleteAnnotation,
      gotoImage,
      togglePlay,
      updateAnnotations
    } = this.props;
    return (
      <div className="col-10 text-center border-right border-left">
        <ImageList
          annotations={annotations}
          currentImage={currentImage}
          drawHotspot={drawHotspot}
          editHotspot={editHotspot}
          imageIndices={imageIndices}
          imageRoot={imageRoot}
          totalImages={totalImages}
          togglePlay={togglePlay}
          deleteAnnotation={deleteAnnotation}
          updateAnnotations={updateAnnotations}
        />
        <Controls
          currentImage={currentImage}
          gotoImage={gotoImage}
          totalImages={totalImages}
        />
      </div>
    );
  }
}

export default PlayArea;
