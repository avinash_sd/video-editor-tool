import React from "react";
import _ from "underscore";
import $ from "jquery";

import Annotations from "./author/Annotations";
import { getAttributes } from "../utils";
import Options from "./author/Options";
import PlayArea from "./author/PlayArea";

const BUFFER = 5;
const INITIAL_FRAMES = 5;
const DEFAULT_FRAME_SPEED = "normal";

class Edit extends React.Component {
  constructor() {
    super();
    this.state = {
      annotations: [],
      bookmarks: [],
      currentImage: 0,
      drawHotspot: false,
      editHotspot: false,
      frameSpeed: DEFAULT_FRAME_SPEED,
      imageRoot: null,
      imageIndices: [],
      isPlaying: false,
      notes: [],
      selectedLanguage: undefined,
      totalImages: 0
    };
  }

  componentDidMount() {
    fetch("/api/videos/" + document.videoId)
      .then(response => response.json())
      .then(data => {
        this.updateVideoData(data);
        this.loadImages();
      });
  }

  updateVideoData = data => {
    const annotations = getAttributes(data.meta_data.frames, "annotation");
    const bookmarks = getAttributes(data.meta_data.frames, "bookmarks");
    const notes = getAttributes(data.meta_data.frames, "notes");
    const speedSettings = _.map(data.meta_data.frames, _f => {
      return { frameSpeed: _f.frames_speed, index: _f.index };
    });
    const selectedLanguage = _.omit(
      _.find(data.languages, language => language.selected),
      "selected"
    );
    this.setState({
      annotations: annotations,
      bookmarks: bookmarks,
      imageRoot: data.meta_data.cdn_url,
      languages: data.languages,
      notes: notes,
      selectedLanguage: selectedLanguage,
      speedSettings: speedSettings,
      totalImages: data.meta_data.count - 1
    });
  };

  gotoImage = index => {
    index =
      index < 0
        ? 0
        : index > this.state.totalImages
        ? this.state.totalImages
        : index;
    this.setState(
      {
        currentImage: index,
        drawHotspot: false,
        editHotspot: false
      },
      () => this.loadImages(index - BUFFER, index + BUFFER)
    );
  };

  jumpToAnnotation = index => {
    this.setState(
      {
        currentImage: index,
        drawHotspot: false,
        editHotspot: false,
        isPlaying: false
      },
      () => this.gotoImage(index)
    );
  };

  loadImages = (start = 0, end = INITIAL_FRAMES) => {
    // handle outbounds
    start = start < 0 ? 0 : start;
    end = (end >= this.state.totalImages ? this.state.totalImages : end) + 1;

    const newImgIndices = _.uniq(
      _.range(start, end).concat(this.state.imageIndices)
    ).sort((a, b) => a - b);

    this.setState(prevState => {
      return {
        imageIndices: newImgIndices
      };
    });
  };

  saveChanges = () => {
    let videoData = {
      count: this.state.totalImages + 1,
      cdn_url: this.state.imageRoot,
      frames: []
    };

    _.each(this.state.annotations, annotation => {
      const index = annotation.index;
      let notes = _.find(this.state.notes, note => note.index === index) || {};

      let bookmarks =
        _.find(this.state.bookmarks, bm => bm.index === index) || {};
      annotation = _.omit(annotation, "index");
      bookmarks = _.omit(bookmarks, "index");
      notes = _.omit(notes, "index");
      const speedSetting = _.find(
        this.state.speedSettings,
        s => s.index === index
      );
      const frameSpeed = speedSetting
        ? speedSetting.frameSpeed
        : DEFAULT_FRAME_SPEED;
      videoData.frames.push({
        annotation: annotation,
        bookmarks: bookmarks,
        frames_speed: frameSpeed,
        index: index,
        notes: notes,
        type: "image"
      });
    });

    fetch("/api/videos/" + document.videoId, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "X-CSRFToken": $("meta[name=csrf-token]").attr("content")
      },
      body: JSON.stringify(videoData)
    })
      .then(response => response.json())
      .then(data => {
        this.updateVideoData(data);
        alert("All changes saved successfully!!");
      });
  };

  togglePlay = e => {
    const annotationList = _.map(this.state.annotations, _a => _a.index);
    const currentFrameSpeed = _.find(
      this.state.speedSettings,
      setting => this.state.currentImage == setting.index
    );
    const frameSpeed = currentFrameSpeed
      ? currentFrameSpeed.frameSpeed
      : DEFAULT_FRAME_SPEED;
    this.setState(
      prevState => {
        return {
          isPlaying: !prevState.isPlaying,
          frameSpeed: frameSpeed
        };
      },
      () => this.playVideo(this.state.currentImage + 1, annotationList)
    );
  };

  getTimeInterval(speed) {
    // setting frame speed
    var intervalTime;
    switch (speed) {
      case "slow":
        intervalTime = 70; // approx value ~15Fps.
        break;
      case "fast":
        intervalTime = 16; // approx value ~60Fps.
        break;
      default:
        intervalTime = 33; // approx value ~30Fps.
    }
    return intervalTime;
  }

  playVideo(startPosition = 0, annotationList = []) {
    if (this.state.isPlaying) {
      this.gotoImage(startPosition);
      if (annotationList.includes(startPosition)) {
        this.setState({
          isPlaying: false
        });
      }
      setTimeout(() => {
        const nextIdx = startPosition + 1;
        if (nextIdx <= this.state.totalImages) {
          this.playVideo(nextIdx, annotationList);
        } else {
          this.setState({
            isPlaying: false
          });
        }
      }, this.getTimeInterval(this.state.frameSpeed));
    }
  }

  toggleDrawHotspot = e => {
    const hasAnnotation = this.isCurrentFrameHasAnnotation();
    this.setState(prevState => {
      let canDraw = !prevState.drawHotspot;
      let canEdit = !prevState.editHotspot;
      if (hasAnnotation) {
        canDraw = false;
        if (prevState.editHotspot) {
          canEdit = false;
        }
      }
      return {
        drawHotspot: canDraw,
        editHotspot: canEdit
      };
    });
  };

  isCurrentFrameHasAnnotation = () => {
    const anotIndex = _.findIndex(
      this.state.annotations,
      _a => _a.index == this.state.currentImage
    );

    return anotIndex !== -1;
  };

  updateAnnotations = annotation => {
    _.extend(annotation, {
      index: this.state.currentImage
    });
    let anotList = this.state.annotations;
    let anotIndex = _.findIndex(
      anotList,
      _a => _a.index == this.state.currentImage
    );
    if (anotIndex !== -1) {
      let _anot = anotList[anotIndex];
      _.extend(_anot, annotation);
    } else {
      anotList.push(annotation);
    }

    anotList = _.sortBy(anotList, "index");

    this.setState({
      annotations: anotList,
      drawHotspot: false,
      editHotspot: true
    });
  };

  deleteAnnotation = () => {
    let anotList = this.state.annotations;
    anotList = _.filter(
      anotList,
      anot => anot.index !== this.state.currentImage
    );

    this.setState({
      annotations: anotList
    });
  };

  updateBookmarks = bookmark => {
    let _bookmark = { index: this.state.currentImage };
    let bookmarkList = this.state.bookmarks;
    if (!bookmark.title && !bookmark.description) {
      bookmarkList = _.filter(
        bookmarkList,
        _bm => _bm.index !== this.state.currentImage
      );
    } else {
      let _bmi = _.findIndex(
        bookmarkList,
        _bm => _bm.index === this.state.currentImage
      );
      if (_bmi === -1) {
        if (bookmark.title) {
          _.extend(_bookmark, { title: {} });
          _bookmark.title[this.state.selectedLanguage.id] = bookmark.title;
        }
        if (bookmark.description) {
          _.extend(_bookmark, { description: {} });
          _bookmark.description[this.state.selectedLanguage.id] =
            bookmark.description;
        }
        bookmarkList.push(_bookmark);
      } else {
        _bookmark = bookmarkList[_bmi];
        if (bookmark.title) {
          _bookmark.title[this.state.selectedLanguage.id] = bookmark.title;
        } else {
          // _.omit not working. Why??
          delete _bookmark.title[this.state.selectedLanguage.id];
        }

        if (bookmark.description) {
          _bookmark.description[this.state.selectedLanguage.id] =
            bookmark.description;
        } else {
          // _.omit not working. Why??
          delete _bookmark.description[this.state.selectedLanguage.id];
        }
        bookmarkList[_bmi] = _bookmark;
      }
    }

    bookmarkList = _.sortBy(bookmarkList, "index");
    this.setState({
      bookmarks: bookmarkList
    });
  };

  updateNotes = note => {
    let notesList = this.state.notes;
    if (!note.text) {
      notesList = _.filter(
        notesList,
        _n => _n.index !== this.state.currentImage
      );
    } else {
      let _ni = _.findIndex(
        notesList,
        _n => _n.index === this.state.currentImage
      );
      let _note = { index: this.state.currentImage };
      if (_ni === -1) {
        _note[this.state.selectedLanguage.id] = note.text;
        notesList.push(_note);
      } else {
        _note = notesList[_ni];
        _note[this.state.selectedLanguage.id] = note.text;
        notesList[_ni] = _note;
      }
    }

    notesList = _.sortBy(notesList, "index");
    this.setState({
      notes: notesList
    });
  };

  updateFrameSpeed = setting => {
    _.extend(setting, {
      index: this.state.currentImage
    });
    let speedSettingList = this.state.speedSettings;
    let settingIndex = _.findIndex(
      speedSettingList,
      _s => _s.index == this.state.currentImage
    );
    if (settingIndex !== -1) {
      let _set = speedSettingList[settingIndex];
      _.extend(_set, setting);
    } else {
      speedSettingList.push(setting);
    }

    speedSettingList = _.sortBy(speedSettingList, "index");

    this.setState({
      speedSettings: speedSettingList
    });
  };

  render() {
    const {
      annotations,
      bookmarks,
      currentImage,
      drawHotspot,
      editHotspot,
      imageRoot,
      isPlaying,
      languages,
      notes,
      selectedLanguage,
      speedSettings,
      totalImages,
      imageIndices
    } = this.state;

    return (
      <div className="row h-100 border">
        <Annotations
          annotations={annotations}
          currentImage={currentImage}
          jumpToAnnotation={this.jumpToAnnotation}
        />
        <PlayArea
          annotations={annotations}
          currentImage={currentImage}
          drawHotspot={drawHotspot}
          editHotspot={editHotspot}
          imageIndices={imageIndices}
          imageRoot={imageRoot}
          totalImages={totalImages}
          gotoImage={this.gotoImage}
          deleteAnnotation={this.deleteAnnotation}
          togglePlay={this.togglePlay}
          updateAnnotations={this.updateAnnotations}
        />
        <Options
          annotations={annotations}
          bookmarks={bookmarks}
          currentImage={currentImage}
          selectedLanguage={selectedLanguage}
          drawHotspot={drawHotspot}
          editHotspot={editHotspot}
          languages={languages}
          notes={notes}
          isPlaying={isPlaying}
          speedSettings={speedSettings}
          totalImages={totalImages}
          gotoImage={this.gotoImage}
          saveChanges={this.saveChanges}
          toggleDrawHotspot={this.toggleDrawHotspot}
          togglePlay={this.togglePlay}
          updateBookmarks={this.updateBookmarks}
          updateNotes={this.updateNotes}
          updateFrameSpeed={this.updateFrameSpeed}
        />
      </div>
    );
  }
}

export { DEFAULT_FRAME_SPEED, Edit };
