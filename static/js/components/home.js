import React from "react";
import $ from "jquery";

import Pagination from "./home/Pagination";
import SearchVideos from "./home/SearchVideos";
import VideoList from "./home/VideoList";
import UploadVideo from "./home/UploadVideo";

function Header() {
  return (
    <header className="p-3 mb-3 text-center">
      <span className="h3 text-capitalize">sandbox authoring tool</span>
    </header>
  );
}

function LoadingPage() {
  return <div>Loading.. Please wait a moment!!</div>;
}

class Home extends React.Component {
  constructor() {
    super();

    this.state = {
      isLoading: false,
      videos: []
    };

    this.loadVideos = this.loadVideos.bind(this);
  }

  searchVideos = args => {
    const searching = args && args.text.length ? true : false;
    this.setState(
      {
        isSearching: searching
      },
      () => this.loadVideos(args)
    );
  };

  loadVideos(args) {
    let url = "/api/videos";
    if (args) {
      const params = $.param(args);
      url = `${url}?${params}`;
    }
    fetch(url)
      .then(response => response.json())
      .then(data => {
        this.setState({
          isLoading: false,
          page: data.page,
          totalPages: data.total_pages,
          videos: data.videos
        });
      });
  }

  componentDidMount() {
    this.setState({
      isLoading: true,
      videos: [],
      page: 1,
      isSearching: false
    });
    this.loadVideos();
  }

  render() {
    const { isSearching, page, totalPages, videos } = this.state;
    const showSearch = isSearching || totalPages > 1;
    const showPaginate = totalPages > 1;
    return (
      <div className="container">
        <Header />
        <UploadVideo loadVideos={this.loadVideos} />
        {showSearch ? <SearchVideos searchVideos={this.searchVideos} /> : null}
        {this.state.isLoading ? (
          <LoadingPage />
        ) : (
          <React.Fragment>
            <VideoList
              videoList={videos}
              totalPages={totalPages}
              currentPage={page}
              loadVideos={this.loadVideos}
            />
            {showPaginate ? (
              <Pagination
                currentPage={page}
                totalPages={totalPages}
                loadVideos={this.loadVideos}
              />
            ) : null}
          </React.Fragment>
        )}
      </div>
    );
  }
}

export default Home;
