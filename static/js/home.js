import React from "react";
import ReactDOM from "react-dom";

import Home from "./components/home";
import "../css/home.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "@fortawesome/fontawesome-free/css/all.css";

ReactDOM.render(<Home />, document.getElementById("root"));
