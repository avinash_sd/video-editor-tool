const CleanWebpackPlugin = require("clean-webpack-plugin");

module.exports = {
  output: {
    path: __dirname + "/static/js/build",
    filename: "[name].pack.js"
  },
  module: {
    rules: [
      {
        use: {
          loader: "babel-loader",
          options: {
            presets: ["babel-preset-env", "babel-preset-react"],
            plugins: ["transform-class-properties"]
          }
        },
        exclude: /node_modules/,
        test: /\.js$/
      },
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"]
      },
      {
        test: /\.(png|woff|woff2|eot|ttf|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: "url-loader?limit=100000"
      }
    ]
  },
  entry: {
    home: __dirname + "/static/js/home.js",
    edit: __dirname + "/static/js/edit.js"
  },
  plugins: [
    new CleanWebpackPlugin(["build"], {
      root: __dirname + "/static/js/" // An absolute path for the root.
    })
  ]
};
