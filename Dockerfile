# Inherit python 2.7 image
FROM python:2.7

# Set an environment variable with the directory
# where we'll be running the app
ENV APP /srv/app

# Create the directory and instruct Docker to operate
# from there from now on
RUN mkdir $APP
WORKDIR $APP

# Install core system packages.
RUN apt-get update && apt-get install -y apt-utils libjpeg-dev libavformat-dev libavfilter-dev\
	libavcodec-dev libavdevice-dev libffi-dev libavutil-dev\
	libswscale-dev libavresample-dev libxmlsec1-dev ffmpeg

# Copy the requirements file in order to install
# Python dependencies
COPY requirements.txt .

# Install Python dependencies
RUN python -m pip install --upgrade pip && python -m pip install --no-cache-dir -r requirements.txt

# We copy the rest of the codebase into the image
COPY . .

RUN mkdir -p /var/log/sandbox
