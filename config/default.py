import os
APP_ROOT_DIR = os.path.dirname(os.path.dirname(__file__))

# Applications default constants
STATIC_FOLDER = os.path.abspath(os.path.join(APP_ROOT_DIR, "static"))
MEDIA_FOLDER = os.path.join(STATIC_FOLDER, "media")

# Exatract image quality
IMAGE_QUALITY = 85

# Celery configuration
CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_BROKER_URL = 'sqla+postgresql://{user}:{password}@{host}:5432/{db}'.format(
    user='sdemos',
    password='sdemos',
    host='celery_db',
    db='sandbox_celery'
)
CELERY_RESULT_BACKEND = 'db+postgresql://{user}:{password}@{host}:5432/{db}'.format(
    user='sdemos',
    password='sdemos',
    host='celery_db',
    db='sandbox_celery'
)
CELERY_IMPORTS = ('tasks', )
CELERY_SEND_TASK_ERROR_EMAILS = True

# Database configuration
SQLALCHEMY_DATABASE_URI = 'postgresql+psycopg2://{user}:{password}@{host}:5432/{db}'.format(
    user='sdemos',
    password='sdemos',
    host='app_db',
    db='sandbox_authoring'
)
SQLALCHEMY_TRACK_MODIFICATIONS = True

# Send file cache timeout in seconds
SEND_FILE_MAX_AGE_DEFAULT = 5

# Rackspace credentials
RACKSPACE_USERNAME = 'avinashsd'
RACKSPACE_API_KEY = 'b611d39f410043538d4cd7f099274093'

# CSRF Token time_limit
WTF_CSRF_TIME_LIMIT = 86400

IMAGE_FORMAT = 'jpg'

MAX_CONTENT_LENGTH = 500 * 1024 * 1024

MAX_RETRIES = 5

FFMPEG_CONVERSION_LOGS_FOLDER = os.path.abspath(
    os.path.join(APP_ROOT_DIR, ".conversion_logs")
)
